package model;

public class BankAccount {
	private int balance;
	
	public BankAccount(){
		balance = 0;
	}
	public BankAccount(int initalBalance){
		balance = initalBalance;
	}
	
	public void deposit(int amount){
		int newBalance = balance + amount;
		balance = newBalance;
	}
	
	public void withdraw(int amount){
		int newBalance = balance - amount;
		balance = newBalance;
	}
	
	public int getBalance(){
		return balance;
	}

}

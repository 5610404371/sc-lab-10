package Frame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;


public class MenuFrame extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	private JPanel panel;
	private JMenuBar menuBar;
	private JMenu selectColor;
	private JMenuItem redButton = new JMenuItem("Red");
	private JMenuItem greenButton = new JMenuItem("Green");
	private JMenuItem blueButton = new JMenuItem("Blue");

	public MenuFrame() {
		createPanel();
		createMenu();
		setBackground();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);

	}

	private void createPanel() {
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		add(panel, BorderLayout.CENTER);

	}
	
	private void createMenu(){
		menuBar = new JMenuBar();
		selectColor = new JMenu("BG Color");
		menuBar.add(selectColor);
		
		selectColor.add(redButton);
		selectColor.add(greenButton);
		selectColor.add(blueButton);
		
		setJMenuBar(menuBar);

	}
	
	public void setBackground(){
		redButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				panel.setBackground(Color.red);
			}
		});
		
		greenButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				panel.setBackground(Color.green);
			}
		});
		
		blueButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				panel.setBackground(Color.blue);
			}
		});
	}

}

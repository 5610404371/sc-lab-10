package Frame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class JButtonFrame extends JFrame{
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	private JPanel panel;
	private JPanel panel1;
	private JButton redButton = new JButton("Red");
	private JButton greenButton = new JButton("Green");
	private JButton blueButton = new JButton("Blue");
	
	public JButtonFrame(){
		
		
		createPanel();
		createButton();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		
	}
	
	private void createPanel(){
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		add(panel,BorderLayout.SOUTH);
		
		panel1 = new JPanel();
		panel.setLayout(new FlowLayout());
		add(panel1,BorderLayout.CENTER);
		
	}
	
	private void createButton(){
		panel.add(redButton);
		panel.add(greenButton);
		panel.add(blueButton);
		
		redButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				panel1.setBackground(Color.red);
			}
		});
		
		greenButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				panel1.setBackground(Color.green);
			}
		});
		
		blueButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				panel1.setBackground(Color.blue);
			}
		});

	}
	
}

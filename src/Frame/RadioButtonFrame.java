package Frame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class RadioButtonFrame extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	private JPanel panel;
	private JPanel panel1;
	private JRadioButton redButton;
	private JRadioButton greenButton;
	private JRadioButton blueButton;

	public RadioButtonFrame() {
		createPanel();
		createButton();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);

	}

	private void createPanel() {
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		add(panel, BorderLayout.SOUTH);

		panel1 = new JPanel();
		panel.setLayout(new FlowLayout());
		add(panel1, BorderLayout.CENTER);

	}

	private void createButton() {
		redButton = new JRadioButton("Red");
		greenButton = new JRadioButton("Green");
		blueButton = new JRadioButton("Blue");
		
		panel.add(redButton);
		panel.add(greenButton);
		panel.add(blueButton);
		
	//	redButton.setSelected(true);

		redButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				panel1.setBackground(Color.red);
			}
		});

		greenButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				panel1.setBackground(Color.green);
			}
		});

		blueButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				panel1.setBackground(Color.blue);
			}
		});

		ButtonGroup group = new ButtonGroup();
		group.add(redButton);
		group.add(greenButton);
		group.add(blueButton);

	}
}

package Frame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class ComboboxFrame extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	private JPanel panel;
	private JPanel panel1;
	private JComboBox<String> combobox;
	private ActionListener listener;
	
	public ComboboxFrame(){
		
		class ChoiceListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setBackground();
			}

		}

		listener = new ChoiceListener();
		createPanel();
		createCombobox();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}
	
	private void createPanel() {
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		add(panel, BorderLayout.CENTER);
		
		panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		add(panel1, BorderLayout.NORTH);
		
	}
	
	private void createCombobox(){
		combobox = new JComboBox<String>();
		combobox.addItem("Red");
		combobox.addItem("Green");
		combobox.addItem("Blue");
		combobox.addActionListener(listener);
		
		panel1.add(combobox);
		
	}
	
	
	private void setBackground(){
		if(combobox.getSelectedItem()== "Red"){
			panel.setBackground(Color.red);
		}
		if(combobox.getSelectedItem()== "Green"){
			panel.setBackground(Color.green);
		}
		if(combobox.getSelectedItem()== "Blue"){
			panel.setBackground(Color.blue);
		}
		
		
	}

}

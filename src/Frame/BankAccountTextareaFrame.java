package Frame;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.BankAccount;


public class BankAccountTextareaFrame extends JFrame{

	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	private static final int AREA_ROWS = 10;
	private static final int AREA_COLOUMS = 20;
			
	private static final int INITIAL_BALANCE = 1000;
	
	private JPanel panel;
	private JPanel panel1;
	private JPanel panel2;
	private JTextField text;
	private JLabel label;
	private JTextArea resultArea;
	private JButton depositButton;
	private JButton withdrawButton;
	private JScrollPane scrollPane;
	private BankAccount account;
	
	public BankAccountTextareaFrame() {
		account = new BankAccount(INITIAL_BALANCE);
		resultArea = new JTextArea("Balance : "+account.getBalance()+"\n",AREA_ROWS,AREA_COLOUMS);
		resultArea.setEditable(false);
		scrollPane = new JScrollPane(resultArea);
		createPanel();
		createTextField();
		createButton();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setResizable(false);
	}

	private void createPanel() {
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		add(panel, BorderLayout.NORTH);

		panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		add(panel1, BorderLayout.SOUTH);

		panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		panel2.add(scrollPane);
		
		add(panel2, BorderLayout.CENTER);
		
		//panel2.add(resultArea);
		//panel2.add(scrollPane);
		
	}

	private void createTextField() {
		label = new JLabel("Input amount : ");
		text = new JTextField(10);


		panel.add(label);
		panel.add(text);
		
	}

	private void createButton() {
		depositButton = new JButton("Deposit");
		withdrawButton = new JButton("Withdraw");

		panel1.add(depositButton);
		panel1.add(withdrawButton);
		
		depositButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent event) {
				account.deposit(Integer.parseInt(text.getText()));
				resultArea.append("---Deposit---"+"\n");
				resultArea.append("Balance : "+account.getBalance()+"\n");
			}
			
		});
		
		withdrawButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent event) {
				if(account.getBalance()>0){
					account.withdraw(Integer.parseInt(text.getText()));
					resultArea.append("---Withdraw---"+"\n");
					resultArea.append("Balance : "+account.getBalance()+"\n");
				}
			}
			
		});

	}
}

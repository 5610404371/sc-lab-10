package Frame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class CheckboxFrame extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 300;
	private JPanel panel;
	private JPanel panel1;
	private ActionListener listener;
	private JCheckBox redButton = new JCheckBox("Red");
	private JCheckBox greenButton = new JCheckBox("Green");
	private JCheckBox blueButton = new JCheckBox("Blue");

	public CheckboxFrame() {

		class ChoiceListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setBackground();
			}

		}

		listener = new ChoiceListener();

		createPanel();
		createButton();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);

	}

	private void createPanel() {
		panel = new JPanel();
		panel.setLayout(new FlowLayout());
		add(panel, BorderLayout.SOUTH);

		panel1 = new JPanel();
		panel.setLayout(new FlowLayout());
		add(panel1, BorderLayout.CENTER);

	}

	private void createButton() {
		panel.add(redButton);
		panel.add(greenButton);
		panel.add(blueButton);

		redButton.addActionListener(listener);
		greenButton.addActionListener(listener);
		blueButton.addActionListener(listener);
	}

	public void setBackground() {
		if (redButton.isSelected()) {
			panel1.setBackground(Color.red);
		}
		if (greenButton.isSelected()) {
			panel1.setBackground(Color.green);
		}
		if (blueButton.isSelected()) {
			panel1.setBackground(Color.blue);
		}
		if (redButton.isSelected() && greenButton.isSelected()) {
			panel1.setBackground(Color.yellow);
		}
		if (redButton.isSelected() && blueButton.isSelected()) {
			panel1.setBackground(Color.magenta);
		}
		if (greenButton.isSelected() && blueButton.isSelected()) {
			panel1.setBackground(Color.cyan);
		}
		if (redButton.isSelected() && greenButton.isSelected()
				&& blueButton.isSelected()) {
			panel1.setBackground(Color.white);
		}
		if (!(redButton.isSelected() || greenButton.isSelected() || blueButton
				.isSelected())) {
			panel1.setBackground(null);
		}
	}

}
